﻿using AliyunDDNS.Models.EventSource;
using AliyunDDNS.Models.Options;
using Furion;
using Furion.EventBus;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AliyunDDNS
{
    public partial class FormAccesskey : Form
    {
        public FormAccesskey()
        {
            InitializeComponent();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("explorer.exe", "https://ram.console.aliyun.com/manage/ak");
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            await MessageCenter.PublishAsync(EventIdEnum.ChangeAccessKey, new ChangeAccessKeyEventSource
            {
                AliyunOptions = new AliyunOptions
                {
                    AccessKey = textBox1.Text.Trim(),
                    AccessKeySecret = textBox2.Text.Trim()
                }
            });
            DialogResult = DialogResult.Yes;
        }

        private void FormAccesskey_Load(object sender, EventArgs e)
        {
            App.Configuration.Reload();
            textBox1.Text = App.Configuration["accessKey"];
            textBox2.Text = App.Configuration["accessKeySecret"];
        }
    }
}
