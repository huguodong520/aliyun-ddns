﻿using AliyunDDNS.Models.EventSource;
using AliyunDDNS.Models.Options;
using Furion;
using Furion.DependencyInjection;
using Furion.EventBus;
using Furion.JsonSerialization;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AliyunDDNS.Common
{
    public class EventSubscriber : IEventSubscriber, ISingleton
    {

        [EventSubscribe(EventIdEnum.ChangeAccessKey)]
        public async Task ChangeAccessKey(EventHandlerExecutingContext context)
        {
            var data = context.Source.Payload as ChangeAccessKeyEventSource;
            if (data?.AliyunOptions == null) return;
            var json = JSON.Serialize(data?.AliyunOptions);
            File.WriteAllText("aliyunsetting.json", json);
            App.Configuration.Reload();
            await Task.CompletedTask;
        }

    }
}
